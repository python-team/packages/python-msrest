python-msrest (0.6.21-5) unstable; urgency=medium

  * Team upload.
  * d/watch: Fix uscan error.
  * Use pybuild-plugin-pyproject.

 -- Colin Watson <cjwatson@debian.org>  Wed, 12 Feb 2025 11:45:10 +0000

python-msrest (0.6.21-4) unstable; urgency=medium

  * Team upload.
  * remove dependency on python3-mock: it's usage is hybrided

 -- Alexandre Detiste <tchet@debian.org>  Tue, 26 Mar 2024 09:06:37 +0100

python-msrest (0.6.21-3) unstable; urgency=medium

  * Team upload.
  * Bump Standards-Version to 4.6.2, no changes
  * Skip more failing tests (Closes: #1058444)
  * Switch to dh-sequence-python3

 -- Luca Boccassi <bluca@debian.org>  Mon, 01 Jan 2024 20:14:59 +0100

python-msrest (0.6.21-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 26 May 2022 15:43:41 +0100

python-msrest (0.6.21-1) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Luca Boccassi ]
  * New upstream version 0.6.21
  * Bump Standards-Version to 4.5.1, no changes
  * Remove dh_make template leftovers from d/watch

 -- Luca Boccassi <bluca@debian.org>  Fri, 29 Jan 2021 13:26:42 +0000

python-msrest (0.6.18-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Luca Boccassi ]
  * New upstream version 0.6.18

 -- Luca Boccassi <bluca@debian.org>  Mon, 07 Sep 2020 14:57:18 +0100

python-msrest (0.6.10-1) unstable; urgency=medium

  [ Luca Boccassi ]
  * Do not run tests that require internet connection at build time
    (Closes: #942101)

  [ Nicolas Dandrimont ]
  * New upstream release
  * Drop python3-trio (Build-)Dependency

 -- Nicolas Dandrimont <olasd@debian.org>  Tue, 19 Nov 2019 18:46:55 +0100

python-msrest (0.6.1-1) unstable; urgency=medium

  * Team upload.

  [ Nicolas Dandrimont ]
  * New upstream version 0.6.1
  * Move from git-dpm to git-buildpackage

  [ Ondřej Nový ]
  * Rename d/tests/control.autodep8 to d/tests/control.
  * Use debhelper-compat instead of debian/compat.

  [ Andrey Rahmatullin ]
  * Drop Python 2 support.

 -- Andrey Rahmatullin <wrar@debian.org>  Sun, 18 Aug 2019 01:36:04 +0500

python-msrest (0.5.5-1) unstable; urgency=medium

  * New upstream version 0.5.5
  * Adopt package in the Debian Python Modules Team (Closes: #899675)
    - Remove Iain from Uploaders, thanks for your past work! (Closes: #889087)
  * Update debian/watch to use git repository with tests
  * Update Vcs-* fields
  * Update to debhelper compat 11.
  * Update to Standards-Version 4.2.1.
  * Add Rules-Requires-Root: no.
  * Add autopkgtests.

 -- Nicolas Dandrimont <olasd@debian.org>  Wed, 07 Nov 2018 15:14:55 +0100

python-msrest (0.4.14-1) unstable; urgency=medium

  * New upstream version 0.4.14
  * debian/control:
   - Updated Standards-Version to 4.1.0
   - Synced versioned dependencies with upstream requirements.txt

 -- Iain R. Learmonth <irl@debian.org>  Thu, 24 Aug 2017 11:04:42 +0100

python-msrest (0.4.5-1) unstable; urgency=medium

  * New upstream release

 -- Antoine R. Dumont (@ardumont) <antoine.romain.dumont@gmail.com>  Fri, 03 Mar 2017 14:39:59 +0100

python-msrest (0.4.4-1) unstable; urgency=medium

  * New upstream release.

 -- Iain R. Learmonth <irl@debian.org>  Tue, 20 Sep 2016 12:54:00 +0100

python-msrest (0.4.3-1) unstable; urgency=medium

  * Initial release. (Closes: #838121)

 -- Antoine R. Dumont (@ardumont) <antoine.romain.dumont@gmail.com>  Sun, 18 Sep 2016 13:58:20 +0100
